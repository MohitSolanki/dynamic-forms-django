from django.db import models

class SingleChoice(models.Model):
	question = models.CharField(max_length=100)
	option_a = models.CharField(max_length=100)
	option_b = models.CharField(max_length=100)
	option_c = models.CharField(max_length=100)
	option_d = models.CharField(max_length=100)

class MultipleChoice(models.Model):
	question = models.CharField(max_length=100)
	option_a = models.CharField(max_length=100)
	option_b = models.CharField(max_length=100)
	option_c = models.CharField(max_length=100)
	option_d = models.CharField(max_length=100)

class BoolChoice(models.Model):
	question = models.CharField(max_length=100) #Only the question is required from the user as answers will be True or False

class TextChoice(models.Model):
	question = models.CharField(max_length=100) #Only the question is required from the user as the answer will be text-based
