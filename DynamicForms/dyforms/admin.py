from django.contrib import admin
from dyforms.models import SingleChoice, MultipleChoice, BoolChoice, TextChoice
# Register your models here.

admin.site.register(SingleChoice)
admin.site.register(MultipleChoice)
admin.site.register(BoolChoice)
admin.site.register(TextChoice)