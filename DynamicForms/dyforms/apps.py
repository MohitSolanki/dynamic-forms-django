from django.apps import AppConfig


class DyformsConfig(AppConfig):
    name = 'dyforms'
