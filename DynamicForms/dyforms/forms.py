from django import forms
from dyforms.models import SingleChoice, MultipleChoice, BoolChoice, TextChoice

#I am using these ModelForms to get the questions and their possible answers from the user

class SingleChoiceForm(forms.ModelForm):
	class Meta:
		model = SingleChoice
		fields = ['question', 'option_a', 'option_b', 'option_c', 'option_d',]

class MultipleChoiceForm(forms.ModelForm):
	class Meta:
		model = MultipleChoice
		fields = ['question', 'option_a', 'option_b', 'option_c', 'option_d',]

class BoolChoiceForm(forms.ModelForm):
	class Meta:
		model = BoolChoice
		fields = ['question',]

class TextChoiceForm(forms.ModelForm):
	class Meta:
		model = TextChoice
		fields = ['question',]								    