from django.shortcuts import render
from django.template.context_processors import csrf
from django.template import RequestContext
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import HttpResponse, HttpResponseRedirect
from dyforms.forms import SingleChoiceForm, MultipleChoiceForm, BoolChoiceForm, TextChoiceForm 
from dyforms.models import SingleChoice, MultipleChoice, BoolChoice, TextChoice
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

@login_required
def index(request):
    SingleChoiceFormSet = formset_factory(SingleChoiceForm, max_num=10)   #Create formsets to generate dynamic forms
    MultipleChoiceFormSet = formset_factory(MultipleChoiceForm, max_num=10)
    BoolChoiceFormSet = formset_factory(BoolChoiceForm, max_num=10)
    TextChoiceFormSet = formset_factory(TextChoiceForm, max_num=10)
    if request.method == 'POST': 
        single_choice_formset = SingleChoiceFormSet(request.POST, request.FILES, prefix='single') #Using prefixes to avoid naming conflicts
        multiple_choice_formset = MultipleChoiceFormSet(request.POST, request.FILES, prefix='multiple')
        bool_choice_formset = BoolChoiceFormSet(request.POST, request.FILES, prefix='bool')
        text_choice_formset = TextChoiceFormSet(request.POST, request.FILES, prefix='texty')

        if single_choice_formset.is_valid() and multiple_choice_formset.is_valid() and bool_choice_formset.is_valid() and text_choice_formset.is_valid():
            for form in single_choice_formset.forms:
                single = SingleChoice()
                single.question = form.cleaned_data['question']
                single.option_a = form.cleaned_data['option_a']
                single.option_b = form.cleaned_data['option_b']
                single.option_c = form.cleaned_data['option_c']
                single.option_d = form.cleaned_data['option_d']
                single.save()
            for form in multiple_choice_formset.forms:
                multiple = MultipleChoice()
                multiple.question = form.cleaned_data['question']
                multiple.option_a = form.cleaned_data['option_a']
                multiple.option_b = form.cleaned_data['option_b']
                multiple.option_c = form.cleaned_data['option_c']
                multiple.option_d = form.cleaned_data['option_d']
                multiple.save()
            for form in bool_choice_formset.forms:
                boolean = BoolChoice()
                boolean.question = form.cleaned_data['question']
                boolean.save()
            for form in text_choice_formset.forms:
                text = TextChoice()
                text.question = form.cleaned_data['question']
                text.save()
                

            return HttpResponseRedirect('/viewform/') # Redirect to the page from where we can view the created form
    else:
        single_choice_formset = SingleChoiceFormSet(prefix='single')
        multiple_choice_formset = MultipleChoiceFormSet(prefix='multiple')
        bool_choice_formset = BoolChoiceFormSet(prefix='bool')
        text_choice_formset = TextChoiceFormSet(prefix='texty')

    return render(request, 'index.html', {'single_choice_formset': single_choice_formset,
                                        'multiple_choice_formset': multiple_choice_formset,
                                        'bool_choice_formset': bool_choice_formset,
                                        'text_choice_formset': text_choice_formset})
@login_required
def form(request):
    single = SingleChoice.objects.all()
    multiple = MultipleChoice.objects.all()
    boolean = BoolChoice.objects.all()
    text = TextChoice.objects.all()

    return render(request, 'view.html', {'single': single,
                                        'multiple': multiple,
                                        'boolean': boolean,
                                        'text': text})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect('/form/')
    else:
        form = UserCreationForm()

    return render(request, 'signup.html', {'form': form})    